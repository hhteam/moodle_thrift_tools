<?php

/* Copyright (C) 2012 Aurelijus Bruzas, CBTec Oy http://cloudberrytec.com/

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */


    return array
    (
        // Change this to point to Moodle installation root
        "moodle_root" => "<moodle installation directory>",

        // Change this to where you want the output files to be put
        "output_dir" => "<directory for generated output>"
    );

?>
