<?php

/* Copyright (C) 2012 Aurelijus Bruzas, CBTec Oy http://cloudberrytec.com/

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */


$config = require(__DIR__ . "/../config.php");

$CFG = new stdClass();
$CFG->libdir = __DIR__;
$CFG->dirroot = $config["moodle_root"];
$CFG->lang = "en";

define("MOODLE_INTERNAL", 1);

require_once __DIR__ . "/../utils.php";

define("PARAM_BOOL", "bool");
define("PARAM_INT", "int");
define("PARAM_FLOAT", "float");
define("PARAM_TEXT", "text");
define("PARAM_PATH", "path");
define("PARAM_FILE", "file");
define("PARAM_ALPHANUMEXT", "alphanumtext");
define("PARAM_RAW", "raw");
define("PARAM_ALPHA", "alpha");
define("PARAM_ALPHANUM", "alphanum");
define("PARAM_ALPHAEXT", "alphatext");
define("PARAM_USERNAME", "username");
define("PARAM_NOTAGS", "notags");
define("PARAM_EMAIL", "email");
define("PARAM_URL", "url");
define("PARAM_PLUGIN", "plugin");
define("PARAM_THEME", "theme");
define("PARAM_SAFEDIR", "safedir");
define("PARAM_TIMEZONE", "timezone");
define("PARAM_CAPABILITY", "capability");
define("PARAM_COMPONENT", "component");
define("PARAM_AREA", "area");
define("PARAM_MULTILANG", "multilang");
define("PARAM_INTEGER", "integet");
define("PARAM_NUMBER", "number");

define("VALUE_DEFAULT", "default");
define("VALUE_OPTIONAL", "optional");
define("NULL_NOT_ALLOWED", "not null");

define("FORMAT_HTML", "html");
define("FORMAT_MOODLE", "moodle");
define("FORMAT_PLAIN", "plain");
define("FORMAT_MARKDOWN", "markdown");

function process_dir($dirName)
{
    global $config;
    global $CFG;

    $dir = opendir($dirName);

    if ($dir === FALSE)
    {
        return;
    }

    while (($entry = readdir($dir)) !== FALSE)
    {
        if (substr($entry, 0, 1) == ".")
        {
            continue;
        }

        if (is_dir($dirName . "/" . $entry))
        {
            process_dir($dirName . "/" . $entry);
        }
        else if ($entry == "externallib.php" && $dirName != $config["moodle_root"] . "/lib")
        {
            include $dirName . "/" . $entry;
        }
    }

    closedir($dir);
}

function process_class($className, $fileName, $idxFile)
{
    global $config;

    print "Processing class: " . $className . "\n";

    $methods = get_class_methods($className);
    $ns = substr($className, 0, -9);
    $now = date("Y-m-d H:i");

    $f = fopen($fileName, "w");

    fwrite($f, <<<EOD
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>$ns</title>
 <link rel="stylesheet" href="style.css">
</head>
<body>
<p>
 [<a href="index.html">Index</a>]
</p>
<h1>$ns</h1>
<p>Client class name: <strong>{$ns}Client</strong></p>
EOD
);

    foreach ($methods as $meth)
    {
        if (substr($meth, -11) != "_parameters" && substr($meth, -8) != "_returns")
        {
            $pars = $meth . "_parameters";
            $rets = $meth . "_returns";

            fwrite($f, "<h2><a name=\"$meth\">$meth</a></h2>\n");
            fwrite($idxFile, "<li><a href=\"" . substr($className, 0, -9) . ".html#$meth\">$meth</a></li>");

            if (in_array($pars, $methods))
            {
                fwrite($f, "<h3>Parameters</h3>\n");
            
                $ps = $className::$pars();

                fwrite($f, "<table>\n");

                foreach ($ps->parameters as $pk => $pv)
                {
                    fwrite($f, "<tr><td><strong>$pk</strong></td><td>" . $pv->getInfo($meth . "Args") . "</td></tr>\n");
                }

                fwrite($f, "</table>");
            }

            if (in_array($rets, $methods))
            {
                $rs = $className::$rets();

                if (is_a($rs, "external_value") || is_a($rs, "external_format_value") ||
                    is_a($rs, "external_single_structure") || is_a($rs, "external_multiple_structure"))
                {
                    fwrite($f, "<h3>Return value</h3>\n");

                    fwrite($f, "<table><tr><td>" . $rs->getInfo($meth . "Ret") . "</td></tr></table>");
                }
            }
        }
    }

    fwrite($f, <<<EOD
<hr>
<p><em>Generated at $now by Doc. Generator. Details: <a href="https://bitbucket.org/hhteam/moodle_thrift_service/wiki/Home">https://bitbucket.org/hhteam/moodle_thrift_service/wiki/Home</a></em></p>
</body>
</html>
EOD
);
    fclose($f);
}

process_dir($config["moodle_root"]);

if (!is_dir($config["output_dir"] . "/doc"))
{
    mkdir($config["output_dir"] . "/doc", 0755, true);
}

file_put_contents($config["output_dir"] . "/doc/style.css", <<<EOD
body { margin: 1em 10%; }
td { vertical-align: top; }
td { background-color: #EEE; }
table { border: 1px solid silver; }
h2 { background-color: #226; color: #FFF; padding: 0.5em; }
EOD
);

$f = fopen($config["output_dir"] . "/doc/index.html", "w");
$now = date("Y-m-d H:i");

fwrite($f, <<<EOT
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Moodle API</title>
 <link rel="stylesheet" href="style.css">
</head>
<body>
<h1>Moodle API</h1>
<ul>
EOT
);

foreach (get_declared_classes() as $cls)
{
    if (substr($cls, -9) == "_external")
    {
        fwrite($f, " <li><a href=\"" . substr($cls, 0, -9) . ".html\"><strong>" . substr($cls, 0, -9) . "</strong></a><ul>\n");

        process_class($cls, $config["output_dir"] . "/doc/" . substr($cls, 0, -9) . ".html", $f);

        fwrite($f, "</ul></li>\n");
    }
}

    fwrite($f, <<<EOD
</ul>
<hr>
<p><em>Generated at $now by Doc. Generator. Details: <a href="https://bitbucket.org/hhteam/moodle_thrift_service/wiki/Home">https://bitbucket.org/hhteam/moodle_thrift_service/wiki/Home</a></em></p>
</body>
</html>
EOD
);

fclose($f);

?>
