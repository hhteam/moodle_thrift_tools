<?php

/* Copyright (C) 2012 Aurelijus Bruzas, CBTec Oy http://cloudberrytec.com/

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */


require_once __DIR__ . "/../utils.php";

class moodle_exception extends Exception
{

};

class external_api
{

};

class external_function_parameters
{
    public $parameters;

    public function __construct($params)
    {
        $this->parameters = $params;
    }
};

class external_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }

    public function getInfo($name)
    {
        return "<em>" . $this->description . "</em><br><tt>" . $this->type . "</tt>";
    }
};

class external_format_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }

    public function getInfo($name)
    {
        return "<em>" . $this->description . "</em><br><tt>" . $this->type . "</tt>";
    }
};

class external_multiple_structure
{
    public $parameters, $description;

    public function __construct($param, $desc = "")
    {
        $this->parameters = $param;
        $this->description = $desc;
    }

    public function getInfo($name)
    {
        if (empty($this->description))
        {
            return "<tt>list </tt>" . $this->parameters->getInfo($name . "Struct");
        }
        else
        {
            return "<em>" . $this->description . "</em><br><tt>list </tt>" . $this->parameters->getInfo($name . "Struct");
        }
    }
};

class external_single_structure
{
    public $parameters;

    public function __construct($param)
    {
        $this->parameters = $param;
    }

    public function getInfo($name)
    {
        $txt = "";

        if (!empty($name))
        {
            $txt .= "<tt>struct " . fix_case($name) . "</tt>: <div style=\"margin-left: 1em;\">";
        }

        $txt .= "<table>";

        foreach ($this->parameters as $pk => $pv)
        {
            $txt .= "<tr><td><strong>$pk</strong></td><td>" . $pv->getInfo(fix_case($name . "_" . $pk)) . "</td></tr>";
        }

        $txt .= "</table>";

        if (!empty($name))
        {
            $txt .= "</div>";
        }

        return $txt;
    }
};

function get_config($name)
{
    if ($name == "moodlecourse")
    {
        $obj = new stdClass();

        $obj->format = FORMAT_PLAIN;
        $obj->showgrades = 0;
        $obj->newsitems = 10;
        $obj->numsections = 10;
        $obj->maxbytes = 1000000;
        $obj->showreports = 1;
        $obj->hiddensections = 0;
        $obj->groupmode = 0;
        $obj->groupmodeforce = 0;

        return $obj;
    }
    else
    {
        return null;
    }
}

?>
