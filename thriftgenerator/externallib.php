<?php

/* Copyright (C) 2012 Aurelijus Bruzas, CBTec Oy http://cloudberrytec.com/

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */


require_once __DIR__ . "/../utils.php";

class moodle_exception extends Exception
{

};

class external_api
{

};

class external_function_parameters
{
    public $parameters;

    public function __construct($params)
    {
        $this->parameters = $params;
    }
};

class external_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        if (!in_array($t, array("bool", "byte", "i16", "i32", "i64", "double", "string")))
        {
            $t = "string";
        }

        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }
};

class external_format_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        if (!in_array($t, array("bool", "byte", "i16", "i32", "i64", "double", "string")))
        {
            $t = "string";
        }

        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }
};

class external_multiple_structure
{
    public $parameters;

    public function __construct($param)
    {
        $this->parameters = $param;
    }

    public function write($file, $name)
    {
        if (is_a($this->parameters, "external_value") || is_a($this->parameters, "external_format_value"))
        {
            fwrite($file, "typedef list<" . $this->parameters->type . "> " . $name . "\n\n");
        }
        else
        {
            $this->parameters->write($file, $name . "Struct");

            fwrite($file, "typedef list<" . $name . "Struct> " . $name . "\n\n");
        }
    }
};

class external_single_structure
{
    public $parameters;

    public function __construct($param)
    {
        $this->parameters = $param;
    }

    public function write($file, $name)
    {
        $entries = array();
        $num = 1;

        foreach ($this->parameters as $pk => $pv)
        {
            if (is_a($pv, "external_single_structure") || is_a($pv, "external_multiple_structure"))
            {
                $pv->write($file, fix_case($name . "_" . $pk));

                $entries[] = "    " . ($num++) . ": " . fix_case($name . "_" . $pk) . " " . $pk;
            }
            else
            {
                $entries[] = "    " . ($num++) . ": " . $pv->type . " " . $pk;
            }
        }

        fwrite($file, "struct " . $name . " {\n". implode(",\n", $entries) . "\n}\n\n");
    }
};

function get_config($name)
{
    if ($name == "moodlecourse")
    {
        $obj = new stdClass();

        $obj->format = FORMAT_PLAIN;
        $obj->showgrades = 0;
        $obj->newsitems = 10;
        $obj->numsections = 10;
        $obj->maxbytes = 1000000;
        $obj->showreports = 1;
        $obj->hiddensections = 0;
        $obj->groupmode = 0;
        $obj->groupmodeforce = 0;

        return $obj;
    }
    else
    {
        return null;
    }
}

?>
