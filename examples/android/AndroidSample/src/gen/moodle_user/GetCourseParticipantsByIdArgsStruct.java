/**
 * Autogenerated by Thrift Compiler (0.9.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package gen.moodle_user;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetCourseParticipantsByIdArgsStruct implements org.apache.thrift.TBase<GetCourseParticipantsByIdArgsStruct, GetCourseParticipantsByIdArgsStruct._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("GetCourseParticipantsByIdArgsStruct");

  private static final org.apache.thrift.protocol.TField USERID_FIELD_DESC = new org.apache.thrift.protocol.TField("userid", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField COURSEID_FIELD_DESC = new org.apache.thrift.protocol.TField("courseid", org.apache.thrift.protocol.TType.I32, (short)2);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new GetCourseParticipantsByIdArgsStructStandardSchemeFactory());
    schemes.put(TupleScheme.class, new GetCourseParticipantsByIdArgsStructTupleSchemeFactory());
  }

  public int userid; // required
  public int courseid; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    USERID((short)1, "userid"),
    COURSEID((short)2, "courseid");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // USERID
          return USERID;
        case 2: // COURSEID
          return COURSEID;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __USERID_ISSET_ID = 0;
  private static final int __COURSEID_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.USERID, new org.apache.thrift.meta_data.FieldMetaData("userid", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.COURSEID, new org.apache.thrift.meta_data.FieldMetaData("courseid", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(GetCourseParticipantsByIdArgsStruct.class, metaDataMap);
  }

  public GetCourseParticipantsByIdArgsStruct() {
  }

  public GetCourseParticipantsByIdArgsStruct(
    int userid,
    int courseid)
  {
    this();
    this.userid = userid;
    setUseridIsSet(true);
    this.courseid = courseid;
    setCourseidIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public GetCourseParticipantsByIdArgsStruct(GetCourseParticipantsByIdArgsStruct other) {
    __isset_bitfield = other.__isset_bitfield;
    this.userid = other.userid;
    this.courseid = other.courseid;
  }

  public GetCourseParticipantsByIdArgsStruct deepCopy() {
    return new GetCourseParticipantsByIdArgsStruct(this);
  }

  @Override
  public void clear() {
    setUseridIsSet(false);
    this.userid = 0;
    setCourseidIsSet(false);
    this.courseid = 0;
  }

  public int getUserid() {
    return this.userid;
  }

  public GetCourseParticipantsByIdArgsStruct setUserid(int userid) {
    this.userid = userid;
    setUseridIsSet(true);
    return this;
  }

  public void unsetUserid() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  /** Returns true if field userid is set (has been assigned a value) and false otherwise */
  public boolean isSetUserid() {
    return EncodingUtils.testBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  public void setUseridIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __USERID_ISSET_ID, value);
  }

  public int getCourseid() {
    return this.courseid;
  }

  public GetCourseParticipantsByIdArgsStruct setCourseid(int courseid) {
    this.courseid = courseid;
    setCourseidIsSet(true);
    return this;
  }

  public void unsetCourseid() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __COURSEID_ISSET_ID);
  }

  /** Returns true if field courseid is set (has been assigned a value) and false otherwise */
  public boolean isSetCourseid() {
    return EncodingUtils.testBit(__isset_bitfield, __COURSEID_ISSET_ID);
  }

  public void setCourseidIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __COURSEID_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case USERID:
      if (value == null) {
        unsetUserid();
      } else {
        setUserid((Integer)value);
      }
      break;

    case COURSEID:
      if (value == null) {
        unsetCourseid();
      } else {
        setCourseid((Integer)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case USERID:
      return Integer.valueOf(getUserid());

    case COURSEID:
      return Integer.valueOf(getCourseid());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case USERID:
      return isSetUserid();
    case COURSEID:
      return isSetCourseid();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof GetCourseParticipantsByIdArgsStruct)
      return this.equals((GetCourseParticipantsByIdArgsStruct)that);
    return false;
  }

  public boolean equals(GetCourseParticipantsByIdArgsStruct that) {
    if (that == null)
      return false;

    boolean this_present_userid = true;
    boolean that_present_userid = true;
    if (this_present_userid || that_present_userid) {
      if (!(this_present_userid && that_present_userid))
        return false;
      if (this.userid != that.userid)
        return false;
    }

    boolean this_present_courseid = true;
    boolean that_present_courseid = true;
    if (this_present_courseid || that_present_courseid) {
      if (!(this_present_courseid && that_present_courseid))
        return false;
      if (this.courseid != that.courseid)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(GetCourseParticipantsByIdArgsStruct other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    GetCourseParticipantsByIdArgsStruct typedOther = (GetCourseParticipantsByIdArgsStruct)other;

    lastComparison = Boolean.valueOf(isSetUserid()).compareTo(typedOther.isSetUserid());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUserid()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.userid, typedOther.userid);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCourseid()).compareTo(typedOther.isSetCourseid());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCourseid()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.courseid, typedOther.courseid);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("GetCourseParticipantsByIdArgsStruct(");
    boolean first = true;

    sb.append("userid:");
    sb.append(this.userid);
    first = false;
    if (!first) sb.append(", ");
    sb.append("courseid:");
    sb.append(this.courseid);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private static class GetCourseParticipantsByIdArgsStructStandardSchemeFactory implements SchemeFactory {
    public GetCourseParticipantsByIdArgsStructStandardScheme getScheme() {
      return new GetCourseParticipantsByIdArgsStructStandardScheme();
    }
  }

  private static class GetCourseParticipantsByIdArgsStructStandardScheme extends StandardScheme<GetCourseParticipantsByIdArgsStruct> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, GetCourseParticipantsByIdArgsStruct struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // USERID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.userid = iprot.readI32();
              struct.setUseridIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // COURSEID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.courseid = iprot.readI32();
              struct.setCourseidIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, GetCourseParticipantsByIdArgsStruct struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(USERID_FIELD_DESC);
      oprot.writeI32(struct.userid);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(COURSEID_FIELD_DESC);
      oprot.writeI32(struct.courseid);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class GetCourseParticipantsByIdArgsStructTupleSchemeFactory implements SchemeFactory {
    public GetCourseParticipantsByIdArgsStructTupleScheme getScheme() {
      return new GetCourseParticipantsByIdArgsStructTupleScheme();
    }
  }

  private static class GetCourseParticipantsByIdArgsStructTupleScheme extends TupleScheme<GetCourseParticipantsByIdArgsStruct> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, GetCourseParticipantsByIdArgsStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetUserid()) {
        optionals.set(0);
      }
      if (struct.isSetCourseid()) {
        optionals.set(1);
      }
      oprot.writeBitSet(optionals, 2);
      if (struct.isSetUserid()) {
        oprot.writeI32(struct.userid);
      }
      if (struct.isSetCourseid()) {
        oprot.writeI32(struct.courseid);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, GetCourseParticipantsByIdArgsStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(2);
      if (incoming.get(0)) {
        struct.userid = iprot.readI32();
        struct.setUseridIsSet(true);
      }
      if (incoming.get(1)) {
        struct.courseid = iprot.readI32();
        struct.setCourseidIsSet(true);
      }
    }
  }

}

