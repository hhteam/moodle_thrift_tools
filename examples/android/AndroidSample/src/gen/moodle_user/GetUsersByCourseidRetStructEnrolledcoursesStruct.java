/**
 * Autogenerated by Thrift Compiler (0.9.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package gen.moodle_user;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetUsersByCourseidRetStructEnrolledcoursesStruct implements org.apache.thrift.TBase<GetUsersByCourseidRetStructEnrolledcoursesStruct, GetUsersByCourseidRetStructEnrolledcoursesStruct._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("GetUsersByCourseidRetStructEnrolledcoursesStruct");

  private static final org.apache.thrift.protocol.TField ID_FIELD_DESC = new org.apache.thrift.protocol.TField("id", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField FULLNAME_FIELD_DESC = new org.apache.thrift.protocol.TField("fullname", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField SHORTNAME_FIELD_DESC = new org.apache.thrift.protocol.TField("shortname", org.apache.thrift.protocol.TType.STRING, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new GetUsersByCourseidRetStructEnrolledcoursesStructStandardSchemeFactory());
    schemes.put(TupleScheme.class, new GetUsersByCourseidRetStructEnrolledcoursesStructTupleSchemeFactory());
  }

  public int id; // required
  public String fullname; // required
  public String shortname; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    ID((short)1, "id"),
    FULLNAME((short)2, "fullname"),
    SHORTNAME((short)3, "shortname");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // ID
          return ID;
        case 2: // FULLNAME
          return FULLNAME;
        case 3: // SHORTNAME
          return SHORTNAME;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __ID_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.ID, new org.apache.thrift.meta_data.FieldMetaData("id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.FULLNAME, new org.apache.thrift.meta_data.FieldMetaData("fullname", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.SHORTNAME, new org.apache.thrift.meta_data.FieldMetaData("shortname", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(GetUsersByCourseidRetStructEnrolledcoursesStruct.class, metaDataMap);
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct() {
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct(
    int id,
    String fullname,
    String shortname)
  {
    this();
    this.id = id;
    setIdIsSet(true);
    this.fullname = fullname;
    this.shortname = shortname;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public GetUsersByCourseidRetStructEnrolledcoursesStruct(GetUsersByCourseidRetStructEnrolledcoursesStruct other) {
    __isset_bitfield = other.__isset_bitfield;
    this.id = other.id;
    if (other.isSetFullname()) {
      this.fullname = other.fullname;
    }
    if (other.isSetShortname()) {
      this.shortname = other.shortname;
    }
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct deepCopy() {
    return new GetUsersByCourseidRetStructEnrolledcoursesStruct(this);
  }

  @Override
  public void clear() {
    setIdIsSet(false);
    this.id = 0;
    this.fullname = null;
    this.shortname = null;
  }

  public int getId() {
    return this.id;
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct setId(int id) {
    this.id = id;
    setIdIsSet(true);
    return this;
  }

  public void unsetId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __ID_ISSET_ID);
  }

  /** Returns true if field id is set (has been assigned a value) and false otherwise */
  public boolean isSetId() {
    return EncodingUtils.testBit(__isset_bitfield, __ID_ISSET_ID);
  }

  public void setIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __ID_ISSET_ID, value);
  }

  public String getFullname() {
    return this.fullname;
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct setFullname(String fullname) {
    this.fullname = fullname;
    return this;
  }

  public void unsetFullname() {
    this.fullname = null;
  }

  /** Returns true if field fullname is set (has been assigned a value) and false otherwise */
  public boolean isSetFullname() {
    return this.fullname != null;
  }

  public void setFullnameIsSet(boolean value) {
    if (!value) {
      this.fullname = null;
    }
  }

  public String getShortname() {
    return this.shortname;
  }

  public GetUsersByCourseidRetStructEnrolledcoursesStruct setShortname(String shortname) {
    this.shortname = shortname;
    return this;
  }

  public void unsetShortname() {
    this.shortname = null;
  }

  /** Returns true if field shortname is set (has been assigned a value) and false otherwise */
  public boolean isSetShortname() {
    return this.shortname != null;
  }

  public void setShortnameIsSet(boolean value) {
    if (!value) {
      this.shortname = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case ID:
      if (value == null) {
        unsetId();
      } else {
        setId((Integer)value);
      }
      break;

    case FULLNAME:
      if (value == null) {
        unsetFullname();
      } else {
        setFullname((String)value);
      }
      break;

    case SHORTNAME:
      if (value == null) {
        unsetShortname();
      } else {
        setShortname((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case ID:
      return Integer.valueOf(getId());

    case FULLNAME:
      return getFullname();

    case SHORTNAME:
      return getShortname();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case ID:
      return isSetId();
    case FULLNAME:
      return isSetFullname();
    case SHORTNAME:
      return isSetShortname();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof GetUsersByCourseidRetStructEnrolledcoursesStruct)
      return this.equals((GetUsersByCourseidRetStructEnrolledcoursesStruct)that);
    return false;
  }

  public boolean equals(GetUsersByCourseidRetStructEnrolledcoursesStruct that) {
    if (that == null)
      return false;

    boolean this_present_id = true;
    boolean that_present_id = true;
    if (this_present_id || that_present_id) {
      if (!(this_present_id && that_present_id))
        return false;
      if (this.id != that.id)
        return false;
    }

    boolean this_present_fullname = true && this.isSetFullname();
    boolean that_present_fullname = true && that.isSetFullname();
    if (this_present_fullname || that_present_fullname) {
      if (!(this_present_fullname && that_present_fullname))
        return false;
      if (!this.fullname.equals(that.fullname))
        return false;
    }

    boolean this_present_shortname = true && this.isSetShortname();
    boolean that_present_shortname = true && that.isSetShortname();
    if (this_present_shortname || that_present_shortname) {
      if (!(this_present_shortname && that_present_shortname))
        return false;
      if (!this.shortname.equals(that.shortname))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(GetUsersByCourseidRetStructEnrolledcoursesStruct other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    GetUsersByCourseidRetStructEnrolledcoursesStruct typedOther = (GetUsersByCourseidRetStructEnrolledcoursesStruct)other;

    lastComparison = Boolean.valueOf(isSetId()).compareTo(typedOther.isSetId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.id, typedOther.id);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetFullname()).compareTo(typedOther.isSetFullname());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetFullname()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.fullname, typedOther.fullname);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetShortname()).compareTo(typedOther.isSetShortname());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetShortname()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.shortname, typedOther.shortname);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("GetUsersByCourseidRetStructEnrolledcoursesStruct(");
    boolean first = true;

    sb.append("id:");
    sb.append(this.id);
    first = false;
    if (!first) sb.append(", ");
    sb.append("fullname:");
    if (this.fullname == null) {
      sb.append("null");
    } else {
      sb.append(this.fullname);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("shortname:");
    if (this.shortname == null) {
      sb.append("null");
    } else {
      sb.append(this.shortname);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private static class GetUsersByCourseidRetStructEnrolledcoursesStructStandardSchemeFactory implements SchemeFactory {
    public GetUsersByCourseidRetStructEnrolledcoursesStructStandardScheme getScheme() {
      return new GetUsersByCourseidRetStructEnrolledcoursesStructStandardScheme();
    }
  }

  private static class GetUsersByCourseidRetStructEnrolledcoursesStructStandardScheme extends StandardScheme<GetUsersByCourseidRetStructEnrolledcoursesStruct> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, GetUsersByCourseidRetStructEnrolledcoursesStruct struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.id = iprot.readI32();
              struct.setIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // FULLNAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.fullname = iprot.readString();
              struct.setFullnameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // SHORTNAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.shortname = iprot.readString();
              struct.setShortnameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, GetUsersByCourseidRetStructEnrolledcoursesStruct struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(ID_FIELD_DESC);
      oprot.writeI32(struct.id);
      oprot.writeFieldEnd();
      if (struct.fullname != null) {
        oprot.writeFieldBegin(FULLNAME_FIELD_DESC);
        oprot.writeString(struct.fullname);
        oprot.writeFieldEnd();
      }
      if (struct.shortname != null) {
        oprot.writeFieldBegin(SHORTNAME_FIELD_DESC);
        oprot.writeString(struct.shortname);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class GetUsersByCourseidRetStructEnrolledcoursesStructTupleSchemeFactory implements SchemeFactory {
    public GetUsersByCourseidRetStructEnrolledcoursesStructTupleScheme getScheme() {
      return new GetUsersByCourseidRetStructEnrolledcoursesStructTupleScheme();
    }
  }

  private static class GetUsersByCourseidRetStructEnrolledcoursesStructTupleScheme extends TupleScheme<GetUsersByCourseidRetStructEnrolledcoursesStruct> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, GetUsersByCourseidRetStructEnrolledcoursesStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetId()) {
        optionals.set(0);
      }
      if (struct.isSetFullname()) {
        optionals.set(1);
      }
      if (struct.isSetShortname()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetId()) {
        oprot.writeI32(struct.id);
      }
      if (struct.isSetFullname()) {
        oprot.writeString(struct.fullname);
      }
      if (struct.isSetShortname()) {
        oprot.writeString(struct.shortname);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, GetUsersByCourseidRetStructEnrolledcoursesStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.id = iprot.readI32();
        struct.setIdIsSet(true);
      }
      if (incoming.get(1)) {
        struct.fullname = iprot.readString();
        struct.setFullnameIsSet(true);
      }
      if (incoming.get(2)) {
        struct.shortname = iprot.readString();
        struct.setShortnameIsSet(true);
      }
    }
  }

}

