/**
 * Autogenerated by Thrift Compiler (0.9.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package gen.core_course;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateCategoriesArgsStruct implements org.apache.thrift.TBase<UpdateCategoriesArgsStruct, UpdateCategoriesArgsStruct._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("UpdateCategoriesArgsStruct");

  private static final org.apache.thrift.protocol.TField ID_FIELD_DESC = new org.apache.thrift.protocol.TField("id", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("name", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField IDNUMBER_FIELD_DESC = new org.apache.thrift.protocol.TField("idnumber", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField PARENT_FIELD_DESC = new org.apache.thrift.protocol.TField("parent", org.apache.thrift.protocol.TType.I32, (short)4);
  private static final org.apache.thrift.protocol.TField DESCRIPTION_FIELD_DESC = new org.apache.thrift.protocol.TField("description", org.apache.thrift.protocol.TType.STRING, (short)5);
  private static final org.apache.thrift.protocol.TField DESCRIPTIONFORMAT_FIELD_DESC = new org.apache.thrift.protocol.TField("descriptionformat", org.apache.thrift.protocol.TType.STRING, (short)6);
  private static final org.apache.thrift.protocol.TField THEME_FIELD_DESC = new org.apache.thrift.protocol.TField("theme", org.apache.thrift.protocol.TType.STRING, (short)7);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new UpdateCategoriesArgsStructStandardSchemeFactory());
    schemes.put(TupleScheme.class, new UpdateCategoriesArgsStructTupleSchemeFactory());
  }

  public int id; // required
  public String name; // required
  public String idnumber; // required
  public int parent; // required
  public String description; // required
  public String descriptionformat; // required
  public String theme; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    ID((short)1, "id"),
    NAME((short)2, "name"),
    IDNUMBER((short)3, "idnumber"),
    PARENT((short)4, "parent"),
    DESCRIPTION((short)5, "description"),
    DESCRIPTIONFORMAT((short)6, "descriptionformat"),
    THEME((short)7, "theme");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // ID
          return ID;
        case 2: // NAME
          return NAME;
        case 3: // IDNUMBER
          return IDNUMBER;
        case 4: // PARENT
          return PARENT;
        case 5: // DESCRIPTION
          return DESCRIPTION;
        case 6: // DESCRIPTIONFORMAT
          return DESCRIPTIONFORMAT;
        case 7: // THEME
          return THEME;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __ID_ISSET_ID = 0;
  private static final int __PARENT_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.ID, new org.apache.thrift.meta_data.FieldMetaData("id", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.NAME, new org.apache.thrift.meta_data.FieldMetaData("name", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.IDNUMBER, new org.apache.thrift.meta_data.FieldMetaData("idnumber", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.PARENT, new org.apache.thrift.meta_data.FieldMetaData("parent", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.DESCRIPTION, new org.apache.thrift.meta_data.FieldMetaData("description", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.DESCRIPTIONFORMAT, new org.apache.thrift.meta_data.FieldMetaData("descriptionformat", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.THEME, new org.apache.thrift.meta_data.FieldMetaData("theme", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(UpdateCategoriesArgsStruct.class, metaDataMap);
  }

  public UpdateCategoriesArgsStruct() {
  }

  public UpdateCategoriesArgsStruct(
    int id,
    String name,
    String idnumber,
    int parent,
    String description,
    String descriptionformat,
    String theme)
  {
    this();
    this.id = id;
    setIdIsSet(true);
    this.name = name;
    this.idnumber = idnumber;
    this.parent = parent;
    setParentIsSet(true);
    this.description = description;
    this.descriptionformat = descriptionformat;
    this.theme = theme;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public UpdateCategoriesArgsStruct(UpdateCategoriesArgsStruct other) {
    __isset_bitfield = other.__isset_bitfield;
    this.id = other.id;
    if (other.isSetName()) {
      this.name = other.name;
    }
    if (other.isSetIdnumber()) {
      this.idnumber = other.idnumber;
    }
    this.parent = other.parent;
    if (other.isSetDescription()) {
      this.description = other.description;
    }
    if (other.isSetDescriptionformat()) {
      this.descriptionformat = other.descriptionformat;
    }
    if (other.isSetTheme()) {
      this.theme = other.theme;
    }
  }

  public UpdateCategoriesArgsStruct deepCopy() {
    return new UpdateCategoriesArgsStruct(this);
  }

  @Override
  public void clear() {
    setIdIsSet(false);
    this.id = 0;
    this.name = null;
    this.idnumber = null;
    setParentIsSet(false);
    this.parent = 0;
    this.description = null;
    this.descriptionformat = null;
    this.theme = null;
  }

  public int getId() {
    return this.id;
  }

  public UpdateCategoriesArgsStruct setId(int id) {
    this.id = id;
    setIdIsSet(true);
    return this;
  }

  public void unsetId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __ID_ISSET_ID);
  }

  /** Returns true if field id is set (has been assigned a value) and false otherwise */
  public boolean isSetId() {
    return EncodingUtils.testBit(__isset_bitfield, __ID_ISSET_ID);
  }

  public void setIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __ID_ISSET_ID, value);
  }

  public String getName() {
    return this.name;
  }

  public UpdateCategoriesArgsStruct setName(String name) {
    this.name = name;
    return this;
  }

  public void unsetName() {
    this.name = null;
  }

  /** Returns true if field name is set (has been assigned a value) and false otherwise */
  public boolean isSetName() {
    return this.name != null;
  }

  public void setNameIsSet(boolean value) {
    if (!value) {
      this.name = null;
    }
  }

  public String getIdnumber() {
    return this.idnumber;
  }

  public UpdateCategoriesArgsStruct setIdnumber(String idnumber) {
    this.idnumber = idnumber;
    return this;
  }

  public void unsetIdnumber() {
    this.idnumber = null;
  }

  /** Returns true if field idnumber is set (has been assigned a value) and false otherwise */
  public boolean isSetIdnumber() {
    return this.idnumber != null;
  }

  public void setIdnumberIsSet(boolean value) {
    if (!value) {
      this.idnumber = null;
    }
  }

  public int getParent() {
    return this.parent;
  }

  public UpdateCategoriesArgsStruct setParent(int parent) {
    this.parent = parent;
    setParentIsSet(true);
    return this;
  }

  public void unsetParent() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __PARENT_ISSET_ID);
  }

  /** Returns true if field parent is set (has been assigned a value) and false otherwise */
  public boolean isSetParent() {
    return EncodingUtils.testBit(__isset_bitfield, __PARENT_ISSET_ID);
  }

  public void setParentIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __PARENT_ISSET_ID, value);
  }

  public String getDescription() {
    return this.description;
  }

  public UpdateCategoriesArgsStruct setDescription(String description) {
    this.description = description;
    return this;
  }

  public void unsetDescription() {
    this.description = null;
  }

  /** Returns true if field description is set (has been assigned a value) and false otherwise */
  public boolean isSetDescription() {
    return this.description != null;
  }

  public void setDescriptionIsSet(boolean value) {
    if (!value) {
      this.description = null;
    }
  }

  public String getDescriptionformat() {
    return this.descriptionformat;
  }

  public UpdateCategoriesArgsStruct setDescriptionformat(String descriptionformat) {
    this.descriptionformat = descriptionformat;
    return this;
  }

  public void unsetDescriptionformat() {
    this.descriptionformat = null;
  }

  /** Returns true if field descriptionformat is set (has been assigned a value) and false otherwise */
  public boolean isSetDescriptionformat() {
    return this.descriptionformat != null;
  }

  public void setDescriptionformatIsSet(boolean value) {
    if (!value) {
      this.descriptionformat = null;
    }
  }

  public String getTheme() {
    return this.theme;
  }

  public UpdateCategoriesArgsStruct setTheme(String theme) {
    this.theme = theme;
    return this;
  }

  public void unsetTheme() {
    this.theme = null;
  }

  /** Returns true if field theme is set (has been assigned a value) and false otherwise */
  public boolean isSetTheme() {
    return this.theme != null;
  }

  public void setThemeIsSet(boolean value) {
    if (!value) {
      this.theme = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case ID:
      if (value == null) {
        unsetId();
      } else {
        setId((Integer)value);
      }
      break;

    case NAME:
      if (value == null) {
        unsetName();
      } else {
        setName((String)value);
      }
      break;

    case IDNUMBER:
      if (value == null) {
        unsetIdnumber();
      } else {
        setIdnumber((String)value);
      }
      break;

    case PARENT:
      if (value == null) {
        unsetParent();
      } else {
        setParent((Integer)value);
      }
      break;

    case DESCRIPTION:
      if (value == null) {
        unsetDescription();
      } else {
        setDescription((String)value);
      }
      break;

    case DESCRIPTIONFORMAT:
      if (value == null) {
        unsetDescriptionformat();
      } else {
        setDescriptionformat((String)value);
      }
      break;

    case THEME:
      if (value == null) {
        unsetTheme();
      } else {
        setTheme((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case ID:
      return Integer.valueOf(getId());

    case NAME:
      return getName();

    case IDNUMBER:
      return getIdnumber();

    case PARENT:
      return Integer.valueOf(getParent());

    case DESCRIPTION:
      return getDescription();

    case DESCRIPTIONFORMAT:
      return getDescriptionformat();

    case THEME:
      return getTheme();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case ID:
      return isSetId();
    case NAME:
      return isSetName();
    case IDNUMBER:
      return isSetIdnumber();
    case PARENT:
      return isSetParent();
    case DESCRIPTION:
      return isSetDescription();
    case DESCRIPTIONFORMAT:
      return isSetDescriptionformat();
    case THEME:
      return isSetTheme();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof UpdateCategoriesArgsStruct)
      return this.equals((UpdateCategoriesArgsStruct)that);
    return false;
  }

  public boolean equals(UpdateCategoriesArgsStruct that) {
    if (that == null)
      return false;

    boolean this_present_id = true;
    boolean that_present_id = true;
    if (this_present_id || that_present_id) {
      if (!(this_present_id && that_present_id))
        return false;
      if (this.id != that.id)
        return false;
    }

    boolean this_present_name = true && this.isSetName();
    boolean that_present_name = true && that.isSetName();
    if (this_present_name || that_present_name) {
      if (!(this_present_name && that_present_name))
        return false;
      if (!this.name.equals(that.name))
        return false;
    }

    boolean this_present_idnumber = true && this.isSetIdnumber();
    boolean that_present_idnumber = true && that.isSetIdnumber();
    if (this_present_idnumber || that_present_idnumber) {
      if (!(this_present_idnumber && that_present_idnumber))
        return false;
      if (!this.idnumber.equals(that.idnumber))
        return false;
    }

    boolean this_present_parent = true;
    boolean that_present_parent = true;
    if (this_present_parent || that_present_parent) {
      if (!(this_present_parent && that_present_parent))
        return false;
      if (this.parent != that.parent)
        return false;
    }

    boolean this_present_description = true && this.isSetDescription();
    boolean that_present_description = true && that.isSetDescription();
    if (this_present_description || that_present_description) {
      if (!(this_present_description && that_present_description))
        return false;
      if (!this.description.equals(that.description))
        return false;
    }

    boolean this_present_descriptionformat = true && this.isSetDescriptionformat();
    boolean that_present_descriptionformat = true && that.isSetDescriptionformat();
    if (this_present_descriptionformat || that_present_descriptionformat) {
      if (!(this_present_descriptionformat && that_present_descriptionformat))
        return false;
      if (!this.descriptionformat.equals(that.descriptionformat))
        return false;
    }

    boolean this_present_theme = true && this.isSetTheme();
    boolean that_present_theme = true && that.isSetTheme();
    if (this_present_theme || that_present_theme) {
      if (!(this_present_theme && that_present_theme))
        return false;
      if (!this.theme.equals(that.theme))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(UpdateCategoriesArgsStruct other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    UpdateCategoriesArgsStruct typedOther = (UpdateCategoriesArgsStruct)other;

    lastComparison = Boolean.valueOf(isSetId()).compareTo(typedOther.isSetId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.id, typedOther.id);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetName()).compareTo(typedOther.isSetName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.name, typedOther.name);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetIdnumber()).compareTo(typedOther.isSetIdnumber());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetIdnumber()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.idnumber, typedOther.idnumber);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetParent()).compareTo(typedOther.isSetParent());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetParent()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.parent, typedOther.parent);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetDescription()).compareTo(typedOther.isSetDescription());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetDescription()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.description, typedOther.description);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetDescriptionformat()).compareTo(typedOther.isSetDescriptionformat());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetDescriptionformat()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.descriptionformat, typedOther.descriptionformat);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetTheme()).compareTo(typedOther.isSetTheme());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetTheme()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.theme, typedOther.theme);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("UpdateCategoriesArgsStruct(");
    boolean first = true;

    sb.append("id:");
    sb.append(this.id);
    first = false;
    if (!first) sb.append(", ");
    sb.append("name:");
    if (this.name == null) {
      sb.append("null");
    } else {
      sb.append(this.name);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("idnumber:");
    if (this.idnumber == null) {
      sb.append("null");
    } else {
      sb.append(this.idnumber);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("parent:");
    sb.append(this.parent);
    first = false;
    if (!first) sb.append(", ");
    sb.append("description:");
    if (this.description == null) {
      sb.append("null");
    } else {
      sb.append(this.description);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("descriptionformat:");
    if (this.descriptionformat == null) {
      sb.append("null");
    } else {
      sb.append(this.descriptionformat);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("theme:");
    if (this.theme == null) {
      sb.append("null");
    } else {
      sb.append(this.theme);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te.getMessage());
    }
  }

  private static class UpdateCategoriesArgsStructStandardSchemeFactory implements SchemeFactory {
    public UpdateCategoriesArgsStructStandardScheme getScheme() {
      return new UpdateCategoriesArgsStructStandardScheme();
    }
  }

  private static class UpdateCategoriesArgsStructStandardScheme extends StandardScheme<UpdateCategoriesArgsStruct> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, UpdateCategoriesArgsStruct struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.id = iprot.readI32();
              struct.setIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.name = iprot.readString();
              struct.setNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // IDNUMBER
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.idnumber = iprot.readString();
              struct.setIdnumberIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // PARENT
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.parent = iprot.readI32();
              struct.setParentIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // DESCRIPTION
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.description = iprot.readString();
              struct.setDescriptionIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 6: // DESCRIPTIONFORMAT
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.descriptionformat = iprot.readString();
              struct.setDescriptionformatIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 7: // THEME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.theme = iprot.readString();
              struct.setThemeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, UpdateCategoriesArgsStruct struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(ID_FIELD_DESC);
      oprot.writeI32(struct.id);
      oprot.writeFieldEnd();
      if (struct.name != null) {
        oprot.writeFieldBegin(NAME_FIELD_DESC);
        oprot.writeString(struct.name);
        oprot.writeFieldEnd();
      }
      if (struct.idnumber != null) {
        oprot.writeFieldBegin(IDNUMBER_FIELD_DESC);
        oprot.writeString(struct.idnumber);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(PARENT_FIELD_DESC);
      oprot.writeI32(struct.parent);
      oprot.writeFieldEnd();
      if (struct.description != null) {
        oprot.writeFieldBegin(DESCRIPTION_FIELD_DESC);
        oprot.writeString(struct.description);
        oprot.writeFieldEnd();
      }
      if (struct.descriptionformat != null) {
        oprot.writeFieldBegin(DESCRIPTIONFORMAT_FIELD_DESC);
        oprot.writeString(struct.descriptionformat);
        oprot.writeFieldEnd();
      }
      if (struct.theme != null) {
        oprot.writeFieldBegin(THEME_FIELD_DESC);
        oprot.writeString(struct.theme);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class UpdateCategoriesArgsStructTupleSchemeFactory implements SchemeFactory {
    public UpdateCategoriesArgsStructTupleScheme getScheme() {
      return new UpdateCategoriesArgsStructTupleScheme();
    }
  }

  private static class UpdateCategoriesArgsStructTupleScheme extends TupleScheme<UpdateCategoriesArgsStruct> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, UpdateCategoriesArgsStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetId()) {
        optionals.set(0);
      }
      if (struct.isSetName()) {
        optionals.set(1);
      }
      if (struct.isSetIdnumber()) {
        optionals.set(2);
      }
      if (struct.isSetParent()) {
        optionals.set(3);
      }
      if (struct.isSetDescription()) {
        optionals.set(4);
      }
      if (struct.isSetDescriptionformat()) {
        optionals.set(5);
      }
      if (struct.isSetTheme()) {
        optionals.set(6);
      }
      oprot.writeBitSet(optionals, 7);
      if (struct.isSetId()) {
        oprot.writeI32(struct.id);
      }
      if (struct.isSetName()) {
        oprot.writeString(struct.name);
      }
      if (struct.isSetIdnumber()) {
        oprot.writeString(struct.idnumber);
      }
      if (struct.isSetParent()) {
        oprot.writeI32(struct.parent);
      }
      if (struct.isSetDescription()) {
        oprot.writeString(struct.description);
      }
      if (struct.isSetDescriptionformat()) {
        oprot.writeString(struct.descriptionformat);
      }
      if (struct.isSetTheme()) {
        oprot.writeString(struct.theme);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, UpdateCategoriesArgsStruct struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(7);
      if (incoming.get(0)) {
        struct.id = iprot.readI32();
        struct.setIdIsSet(true);
      }
      if (incoming.get(1)) {
        struct.name = iprot.readString();
        struct.setNameIsSet(true);
      }
      if (incoming.get(2)) {
        struct.idnumber = iprot.readString();
        struct.setIdnumberIsSet(true);
      }
      if (incoming.get(3)) {
        struct.parent = iprot.readI32();
        struct.setParentIsSet(true);
      }
      if (incoming.get(4)) {
        struct.description = iprot.readString();
        struct.setDescriptionIsSet(true);
      }
      if (incoming.get(5)) {
        struct.descriptionformat = iprot.readString();
        struct.setDescriptionformatIsSet(true);
      }
      if (incoming.get(6)) {
        struct.theme = iprot.readString();
        struct.setThemeIsSet(true);
      }
    }
  }

}

