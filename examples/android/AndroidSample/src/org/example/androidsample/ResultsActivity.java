package org.example.androidsample;

import gen.core_course.GetCoursesArgs;
import gen.core_course.GetCoursesRetStruct;
import gen.core_course.core_course;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TTransport;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.EditText;

public class ResultsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_results, menu);
        return true;
    }
    
    @Override
    protected void onStart()
    {
    	super.onStart();
    	
    	Intent i = getIntent();
    	
    	String moodleUrl = i.getStringExtra("moodle_url");
    	
    	if (!moodleUrl.endsWith("/"))
    	{
    		moodleUrl += "/";
    	}
    	
    	String authUrl = moodleUrl + "login/token.php?username=" + i.getStringExtra("username") + "&password=" + i.getStringExtra("password") + "&service=moodle_mobile_app";    	
    	InputStream is = null;
    	DefaultHttpClient httpClient = new DefaultHttpClient();
    	HttpGet httpGet = new HttpGet(authUrl);
    	
    	try
    	{
    		HttpResponse response = httpClient.execute(httpGet);
    		
    		is = response.getEntity().getContent();
    	}
    	catch (Exception e)
    	{ }
    	
    	String token = "";

    	if (is != null)
    	{
        	String json = "";
        	JSONObject obj = null;

    		try
    		{
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
            
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "n");
                }
                
                is.close();
                json = sb.toString();
                obj = new JSONObject(json);
                
                token = obj.getString("token");
            }
    		catch (Exception e)
    		{
                
            }    		
    	}

    	if (token != "")
    	{
    		try
        	{
        		TTransport transport = new THttpClient(moodleUrl + "/local/thrift/core_course.php?token=" + token);
        		TProtocol protocol = new TJSONProtocol(transport);
        		
        		core_course.Client client = new core_course.Client(protocol);
        		
        		List<GetCoursesRetStruct> result = client.get_courses(new GetCoursesArgs());
        		        
        		transport.close();
        		
        		EditText output = (EditText) findViewById(R.id.editText1);
        		
        		for (GetCoursesRetStruct res : result)
        		{
        			output.getText().append(res.getFullname() + "\n");
        		}
        	}
        	catch (Exception e)
        	{
        		
    		}
    	}
    }
}
