package org.example.androidsample;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void clicked(View view)
    {
    	Intent i = new Intent(getApplicationContext(), ResultsActivity.class);
    	
    	i.putExtra("moodle_url", ((EditText) findViewById(R.id.editText1)).getText().toString());
    	i.putExtra("username", ((EditText) findViewById(R.id.editText2)).getText().toString());
    	i.putExtra("password", ((EditText) findViewById(R.id.editText3)).getText().toString());
    	
    	startActivity(i);
	}
}
