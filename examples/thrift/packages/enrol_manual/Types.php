<?php
namespace enrol_manual;

/**
 * Autogenerated by Thrift Compiler (0.9.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Exception\TApplicationException;


class ApiError extends TException {
  static $_TSPEC;

  public $errorCode = null;
  public $humanMessage = null;
  public $userData = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'errorCode',
          'type' => TType::I32,
          ),
        2 => array(
          'var' => 'humanMessage',
          'type' => TType::STRING,
          ),
        3 => array(
          'var' => 'userData',
          'type' => TType::MAP,
          'ktype' => TType::STRING,
          'vtype' => TType::STRING,
          'key' => array(
            'type' => TType::STRING,
          ),
          'val' => array(
            'type' => TType::STRING,
            ),
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['errorCode'])) {
        $this->errorCode = $vals['errorCode'];
      }
      if (isset($vals['humanMessage'])) {
        $this->humanMessage = $vals['humanMessage'];
      }
      if (isset($vals['userData'])) {
        $this->userData = $vals['userData'];
      }
    }
  }

  public function getName() {
    return 'ApiError';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->errorCode);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->humanMessage);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::MAP) {
            $this->userData = array();
            $_size0 = 0;
            $_ktype1 = 0;
            $_vtype2 = 0;
            $xfer += $input->readMapBegin($_ktype1, $_vtype2, $_size0);
            for ($_i4 = 0; $_i4 < $_size0; ++$_i4)
            {
              $key5 = '';
              $val6 = '';
              $xfer += $input->readString($key5);
              $xfer += $input->readString($val6);
              $this->userData[$key5] = $val6;
            }
            $xfer += $input->readMapEnd();
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('ApiError');
    if ($this->errorCode !== null) {
      $xfer += $output->writeFieldBegin('errorCode', TType::I32, 1);
      $xfer += $output->writeI32($this->errorCode);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->humanMessage !== null) {
      $xfer += $output->writeFieldBegin('humanMessage', TType::STRING, 2);
      $xfer += $output->writeString($this->humanMessage);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->userData !== null) {
      if (!is_array($this->userData)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('userData', TType::MAP, 3);
      {
        $output->writeMapBegin(TType::STRING, TType::STRING, count($this->userData));
        {
          foreach ($this->userData as $kiter7 => $viter8)
          {
            $xfer += $output->writeString($kiter7);
            $xfer += $output->writeString($viter8);
          }
        }
        $output->writeMapEnd();
      }
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class EnrolUsersArgsStruct {
  static $_TSPEC;

  public $roleid = null;
  public $userid = null;
  public $courseid = null;
  public $timestart = null;
  public $timeend = null;
  public $suspend = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'roleid',
          'type' => TType::I32,
          ),
        2 => array(
          'var' => 'userid',
          'type' => TType::I32,
          ),
        3 => array(
          'var' => 'courseid',
          'type' => TType::I32,
          ),
        4 => array(
          'var' => 'timestart',
          'type' => TType::I32,
          ),
        5 => array(
          'var' => 'timeend',
          'type' => TType::I32,
          ),
        6 => array(
          'var' => 'suspend',
          'type' => TType::I32,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['roleid'])) {
        $this->roleid = $vals['roleid'];
      }
      if (isset($vals['userid'])) {
        $this->userid = $vals['userid'];
      }
      if (isset($vals['courseid'])) {
        $this->courseid = $vals['courseid'];
      }
      if (isset($vals['timestart'])) {
        $this->timestart = $vals['timestart'];
      }
      if (isset($vals['timeend'])) {
        $this->timeend = $vals['timeend'];
      }
      if (isset($vals['suspend'])) {
        $this->suspend = $vals['suspend'];
      }
    }
  }

  public function getName() {
    return 'EnrolUsersArgsStruct';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->roleid);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->userid);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->courseid);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 4:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->timestart);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 5:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->timeend);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 6:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->suspend);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('EnrolUsersArgsStruct');
    if ($this->roleid !== null) {
      $xfer += $output->writeFieldBegin('roleid', TType::I32, 1);
      $xfer += $output->writeI32($this->roleid);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->userid !== null) {
      $xfer += $output->writeFieldBegin('userid', TType::I32, 2);
      $xfer += $output->writeI32($this->userid);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->courseid !== null) {
      $xfer += $output->writeFieldBegin('courseid', TType::I32, 3);
      $xfer += $output->writeI32($this->courseid);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->timestart !== null) {
      $xfer += $output->writeFieldBegin('timestart', TType::I32, 4);
      $xfer += $output->writeI32($this->timestart);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->timeend !== null) {
      $xfer += $output->writeFieldBegin('timeend', TType::I32, 5);
      $xfer += $output->writeI32($this->timeend);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->suspend !== null) {
      $xfer += $output->writeFieldBegin('suspend', TType::I32, 6);
      $xfer += $output->writeI32($this->suspend);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}


