(function()
{
	var moodleRoot = "http://<your moodle>",
		token = "<token>"

	var courseClient = new core_courseClient(new Thrift.Protocol(
		new Thrift.Transport(moodleRoot + "/local/thrift/core_course.php?token=" + token)))

	var arg = new GetCoursesArgs()

	arg.ids = []

	try
	{
		var res = courseClient.get_courses(arg)

		for (var i in res)
		{
			console.log(i + ": " + res[i].id + " - " + res[i].shortname + " - " + res[i].fullname)
		}
	}
	catch (e)
	{
		console.log("We have an error!")
	}
})();
