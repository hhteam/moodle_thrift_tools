<?php

/* Copyright (C) 2012 Aurelijus Bruzas, CBTec Oy http://cloudberrytec.com/

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */


class moodle_exception extends Exception
{

};

class external_api
{

};

class external_function_parameters
{
    public $parameters;

    public function __construct($params)
    {
        $this->parameters = $params;
    }
};

class external_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        if (!in_array($t, array("bool", "byte", "i16", "i32", "i64", "double", "string")))
        {
            $t = "string";
        }

        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }
};

class external_format_value
{
    public $type, $description, $defValue;

    public function __construct($t, $desc = "", $def = null)
    {
        if (!in_array($t, array("bool", "byte", "i16", "i32", "i64", "double", "string")))
        {
            $t = "string";
        }

        $this->type = $t;
        $this->description = $desc;
        $this->defValue = $def;
    }
};

class external_multiple_structure
{
    public $parameters;

    public function __construct($param)
    {
        $this->parameters = $param;
    }

    public function write($aname)
    {
        if (is_a($this->parameters, "external_value") || is_a($this->parameters, "external_format_value"))
        {
            return "\$$aname";
        }
        else
        {
            return $this->parameters->write($aname);
        }
    }

    public function preWrite($aname)
    {
        if (is_a($this->parameters, "external_value") || is_a($this->parameters, "external_format_value"))
        {
            return "";
        }
        else
        {
            return $this->parameters->preWrite($aname);
        }
    }

    public function writeRes($name, $fromName, $ns, $type)
    {
        if (is_a($this->parameters, "external_value") || is_a($this->parameters, "external_format_value"))
        {
            return "\n        \$$name = \$$fromName;\n";
        }
        else
        {
            $idx = preg_replace("/[^A-Za-z0-9]/", "", $fromName) . "I";

            $txt = "        \${$name} = array();\n\n        foreach (\$$fromName as \$$idx)\n      {\n";
            $txt .= $this->parameters->writeRes($name . "Obj", $idx, $ns, $type . "Struct");
            $txt .= "        \${$name}[] = \${$name}Obj;\n";
            $txt .= "      }\n\n";

            return $txt;
        }
    }
};

class external_single_structure
{
    public $parameters;

    public function __construct($param)
    {
        $this->parameters = $param;
    }

    public function write($aname)
    {
        $entries = array();
        $num = 1;

        foreach ($this->parameters as $pk => $pv)
        {
            if (is_a($pv, "external_single_structure"))
            {
                $entries[] = "            \"$pk\" => " . $pv->write($aname);
            }
            else if (is_a($pv, "external_multiple_structure"))
            {
                if (is_a($pv->parameters, "external_value") || is_a($pv->parameters, "external_format_value"))
                {
                    $entries[] = "            \"$pk\" => \$$aname->$pk";
                }
                else
                {
                    //$entries[] = "            \"$pk\" => \$$aname" . ucfirst($pk) . "Val";
                    $entries[] = "            \"$pk\" => \"TODO\"";
                }
            }
            else
            {
                $entries[] = "            \"$pk\" => \$$aname->$pk";
            }
        }

        return "array(\n" . implode(",\n", $entries) . ")";
    }

    public function preWrite($aname)
    {
        $txt = "";

        foreach ($this->parameters as $pk => $pv)
        {
            if (is_a($pv, "external_multiple_structure"))
            {
                if (is_a($pv->parameters, "external_multiple_structure"))
                {
                    $pv->parameters->preWrite($aname);
                }
                else if (is_a($pv->parameters, "external_single_structure"))
                {
                
                }
            }
        }

        return $txt;
    }

    public function writeRes($name, $fromName, $ns, $type)
    {
        $txt = "        \${$name} = new \\$ns\\{$type}();\n\n";

        foreach ($this->parameters as $pk => $pv)
        {
            if (is_a($pv, "external_single_structure") || is_a($pv, "external_multiple_structure"))
            {
                $txt .= "\n" . $pv->writeRes($name . ucfirst($pk), "{$fromName}[\"$pk\"]", $ns, $type . ucfirst($pk));

                $txt .= "        \$$name->$pk = \$" . $name . ucfirst($pk) . ";\n";
            }
            else
            {
                $txt .= "        \$$name->$pk = \${$fromName}[\"$pk\"];\n";
            }
        }

        return $txt;
    }
};

function get_config($name)
{
    if ($name == "moodlecourse")
    {
        $obj = new stdClass();

        $obj->format = FORMAT_PLAIN;
        $obj->showgrades = 0;
        $obj->newsitems = 10;
        $obj->numsections = 10;
        $obj->maxbytes = 1000000;
        $obj->showreports = 1;
        $obj->hiddensections = 0;
        $obj->groupmode = 0;
        $obj->groupmodeforce = 0;

        return $obj;
    }
    else
    {
        return null;
    }
}

?>
